This module emerged from https://www.drupal.org/node/661198

For now it defines permissions for every Filefield Source. 
In theory it could be expanded in a much more granular way.

Authored by W. Vanheste / rv0
https://www.drupal.org/user/655596